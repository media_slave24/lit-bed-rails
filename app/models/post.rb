class Post < ActiveRecord::Base

  acts_as_commentable
  self.per_page = SiteConfig.posts.posts_per_page

  GENRES = %w{
    prose
    poetry
  }

  TYPES = %w{ 
    opus 
    news
  }

  # Associations
  belongs_to :user
  has_many :ratings
  before_save :set_user_rating
  has_many :bookmarks

  # Validations
  validates :title, :content, :genre, :presence => true
  validates_inclusion_of :genre, :in => GENRES

  # Attributes
  attr_accessible :annotation, :content, 
                  :title, :genre, :published

  before_validation do |record|
    record.post_type ||= "opus"
  end

  # Scopes
  scope :published, 
        ->{ where(published: true) }
  scope :today, 
        ->{ where(created_at: Time.new.beginning_of_day..Time.new.end_of_day) }
  scope :yesterday, 
        ->{ where(created_at: Time.new.yesterday.beginning_of_day..Time.new.yesterday.end_of_day ) }
  scope :this_week, 
        ->{ where(created_at: Time.new.beginning_of_week..Time.new.end_of_week) }
  scope :genre,
        ->(genre) { where(genre: genre) }
  scope :by_rating,
        ->{ order('rating DESC') }
  scope :recent,
        ->{ order('created_at DESC') }

  # Instance methods
  def genres_for_select
    GENRES.map { |g|
      [ (block_given? ? yield(g) : g.humanize), g ]
    }
  end

  def belongs_to?(user)
    return false unless user
    self.user.id == user.id
  end

  def i18n_genre
    I18n.t("common.#{genre}")
  end

  def vote(vote, user_id)
    ratings.create vote: vote, user_id: user_id
    self.rating = ratings.sum :vote
    save
    return self.rating
  end

  private

  def set_user_rating
    if self.rating_changed?
      self.user.update_attribute :rating, (self.user.posts_rating_sum + self.rating)
    end
  end

end
