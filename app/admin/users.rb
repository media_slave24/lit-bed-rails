ActiveAdmin.register User do
  index do
    column :id
    column :email
    column :nickname
    column :fullname
    column :rating
    column :role
  end  
end
