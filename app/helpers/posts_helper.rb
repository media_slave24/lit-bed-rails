module PostsHelper

  def link_to_sorted(name, opts={})
    opts.stringify_keys!
    if_current_state = !!opts.
      find { |k,v| params[:sort] && params[:sort][k] == v }
    html_opts = if_current_state ? 
      { class: 'current' } : {}
    html_opts = opts.delete('html').merge(html_opts) if opts['html']
    url_opts = params[:sort] ? params[:sort].merge(opts) : opts
    link_to name, posts_path(:sort => url_opts), html_opts
  end

  def user_voted?(post)
    user_signed_in? && current_user.voted?(post)
  end

  def rate_up_link(post)
    if !user_voted?(post)
      link_to '+', rate_post_up_path(post), :method => :post, remote: true, class: 'rate_up'
    end
  end

  def rate_down_link(post)
    if !user_voted?(post)
      link_to '-', rate_post_down_path(post), :method => :post, remote: true, class: 'rate_down'
    end
  end

  def one_post_page?
    params[:action] == 'show'
  end
end
