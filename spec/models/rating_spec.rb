require 'spec_helper'

describe Rating do
  context "valid factory" do
    subject { build :rating }
    it { should be_valid }
  end

  context "without post_id" do
    subject { build :rating, post_id: nil }
    it { should_not be_valid }
  end

  context "without user_id" do
    subject { build :rating, user_id: nil }
    it { should_not be_valid }
  end

  context "when one user voted twice" do
    let(:post) { create :post }
    before {
      create :rating, user_id: post.user.id, post_id: post.id
    }
    subject { build :rating, user_id: post.user.id, post_id: post.id }

    it { should_not be_valid }
  end

  context "with vote == 0" do
    subject { build :rating, vote: 0 }
    it { should_not be_valid }
  end
end
