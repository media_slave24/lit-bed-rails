// Vote popup
(function(){
  if($('.more').length > 0) {
    $('.more').click(function(){
      var text = $(this).parentsUntil("#post, .post", ".wrap").find(".text");
      var closedHeight = text.hasClass("opened") ? 0 : text.height()
      text.removeAttr("style").toggleClass("opened", 300);

      if(closedHeight != 0 && closedHeight >= text.height()) {
        $(this).remove();
      }
    });
    $('.more').hide();
    $(".wrap").hover(
        function() {
          $(this).find(".more").show();
        },
        function() {
          $(this).find(".more").hide();
        }
      )
  
  }

  var delay = 500;
  var inDelay = 200;
  $('.wrap').hover(
    function(){
      var vote   = $(this).parentsUntil('#content', ".post, #post").find('.vote'),
          left   = $(this).offset().left + $(this).width(),
          top    = $(this).offset().top;

      clearTimeout(vote.data('timeoutId'));

      vote.css({
        // display: "block",
        position: "absolute",
        left: left,
        top: top,
        "z-index": 10000
      });
      vote.slideDown(inDelay);
    },
    function(e){
      var vote = $(this).parentsUntil('#content', '.post, #post').find('.vote');
      vote.data('timeoutId', setTimeout(function(){ vote.fadeOut(inDelay); }, delay));
    });

  $('.vote').hover(
    function(){
      clearTimeout($(this).data('timeoutId'));
    }, 
    function(){
      var vote = $(this);
      vote.data('timeoutId', setTimeout(function(){ vote.fadeOut(inDelay); }, delay));
  });
})();
