class User::Provider < ActiveRecord::Base
  belongs_to :user
  attr_accessible :provider, :uid, :user_id
  validates_uniqueness_of :provider, scope: :user_id
end
