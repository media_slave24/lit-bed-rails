class CustomLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_formatted_s(:db)} #{severity} #{msg}\n"
  end
end
logfile = File.open("#{Rails.root}/log/app_log_#{Rails.env}.log", 'a')
logfile.sync = true
APP_LOGGER = CustomLogger.new(logfile)
