class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def facebook
    strategy!
  end

  def twitter
    strategy!
  end

  def vkontakte
    strategy!
  end

  private

  def strategy!
    auth = request.env['omniauth.auth']
    provider = User::Provider.find_by_provider_and_uid(auth.provider, auth.uid)
    
    if !provider && !user_signed_in?
      return redirect_to new_user_session_path(login_for_omniauth: true, provider: auth.provider)
    elsif user_signed_in?
      session['devise.social_data'] = nil;
      provider = current_user.
                  providers.
                  find_or_create_by_provider_and_uid provider: auth.provider, uid: auth.uid
    end

    set_flash_message(:notice, :success) if is_navigational_format?
    sign_in_and_redirect provider.user, event: :authentication
  end
end
