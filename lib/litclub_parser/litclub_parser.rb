#coding: utf-8

class LitclubParser
  extend ActiveSupport::Autoload

  Page = Struct.new(:parsed, :html)
  User = Struct.new(:nickname, :fullname, :prose, :poetry, :else)
  Post = Struct.new(:title, :content, :id, :link)

  autoload :ProfileParser
  autoload :PostParser

  @@host = "http://litclub.org.ua"

  attr_accessor :user_pages
  attr_accessor :users

  attr_accessor :post_pages
  attr_accessor :posts

  def initialize
    self.user_pages = {}
    self.users = {}
    self.post_pages = {}
    self.posts = {}
  end

  def user_info(nickname)
    return users[nickname] if users[nickname]

    html = profile(nickname)
    parsed = Nokogiri::HTML(html)
    page = Page.new(parsed, html)

    profile_parser = ProfileParser.new(page)
    user = User.new(nickname, *profile_parser.info)

    self.user_pages[nickname] = page
    self.users[nickname] = user

    user
  end

  def post_info(id)
    return posts[id] if posts[id]
    html = post(id)
    parsed = Nokogiri::HTML(html)
    page = Page.new(parsed, html)

    post_parser = PostParser.new(page)
    post = Post.new(*post_parser.info, id)

    self.post_pages[id] = page
    self.posts[id] = post
  end

  def profile(nick)
    html("/users/profile/#{nick}/")
  end

  def post(id)
    html("/texts/show/#{id}/")
  end

  private
  def html(path)
    open(URI(@@host + path)).read
  end
end
