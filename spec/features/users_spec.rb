require 'spec_helper'

describe "Users" do
  include SignInFeature
  context "registration" do
    let!(:password) { "asd[]125Q*&$1" }
    let!(:nickname) { "aksdjjas" }
    before {
      User.destroy_all
      visit new_user_registration_path
    }
    it {
      within("#registration form") do
        fill_in "user[nickname]", with: nickname
        fill_in "user[email]", with: "email@example.com"
        fill_in "user[fullname]", with: "SomeFucking COOL FULLNAME"
        fill_in "user[password]", with: password
        fill_in "user[password_confirmation]", with: password
        find("input[type=submit]").click
      end
      
      current_path.should_not eq(user_registration_path)

      visit root_path

      page.should_not have_css("a[href='/users/register']")
      page.should have_css("a[href='/users/glogout']")
      User.last.nickname.should eq(nickname) 
    }
  end
end

