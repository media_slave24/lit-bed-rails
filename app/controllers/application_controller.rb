class ApplicationController < ActionController::Base
  layout 'main'
  protect_from_forgery
  hide_action :not_found

  # if %w{ development test}.include? Rails.env
  #   around_filter :write_logs
  # end

  if Rails.env == 'production'
    rescue_from ::ActiveRecord::RecordNotFound, with: :render_404
  end

  def abc
    render inline: request.ip
  end

  def reload_settings
    SiteConfig.reload!
    redirect_to root_path
  end

  def render_404
    APP_LOGGER.error "404 rendered"
    render file: "shared/404"#, layout: false
  end

  def write_logs
    APP_LOGGER.debug "Processing #{params[:controller]}##{params[:action]}"<<
    "\n  by #{ current_user ? current_user.to_yaml : 'guest' }"
    yield
    APP_LOGGER.debug "Request completed"
  end
end
