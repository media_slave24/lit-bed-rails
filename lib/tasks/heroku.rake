namespace :heroku do
  task :deploy do
    %x{\
      cd #{Rails.root} &&\
      git stash &&\
      git checkout master &&\
      rm -rf public/assets &&\
      rake assets:precompile RAILS_ENV=development &&\
      git add -u public/assets  &&\
      git add public/assets &&\
      git commit -m 'Precompiled assets' &&\
      git push heroku
    }
  end
end
