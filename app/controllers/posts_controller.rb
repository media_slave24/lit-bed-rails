class PostsController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]

  # Actions with views

  def index
    @posts = Post.
              published.
              recent.
              paginate(:page => params[:page])
  end

  def user_posts
    @user = User.find(params[:user_id])
    @posts = @user.posts
  end

  def bookmarks
    @posts = current_user.posts_bookmarks
  end

  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = current_user.posts.build
  end

  def edit
    @post = current_user.posts.find(params[:id])
  end

  # Actions with json-response

  def vote
    post = Post.find(params[:id])
    if current_user.voted?(params[:id])
      flash[:notice] = t('common.voted_already')
      return redirect_to post
    end
    user_id = current_user.id.to_i
    count = current_user.voting_level.to_i
    count = -count if params[:minus]

    rating = post.vote(count, user_id) 

    if request.xhr?
      render text: { rating: rating }.to_json
    else
      flash[:notice] = t('common.vote_accepted')
      redirect_to post
    end
  end

  def comment
    post = Post.find(params[:id]) 
    body = params[:comment] ? params[:comment][:body] : nil
    Comment.build_from(post, current_user.id, body).save

    if request.xhr?
      render text: { comment: comment.body }.to_json
    else
      redirect_to :back, notice: t('common.comment_writed')
    end
  end

  def create_bookmark
    current_user.
      bookmarks.
      create post_id: params[:id] if params[:id]
    if request.xhr?
      render text: { msg: "Bookmark created" }
    else
      flash[:notice] = t('common.bookmark_created')
      redirect_to :back
    end
  end

  def destroy_bookmark
    current_user.
      bookmarks.
      find_by_post_id(params[:id]).
      destroy
    if request.xhr?
      render text: { msg: "Bookmark destroyed" }
    else
      flash[:notice] = t('common.bookmark_destroyed')
      redirect_to :back
    end
  end

  # Actions without views

  def create
    @post = current_user.
            posts.
            build(params[:post])

    if @post.save
      redirect_to @post, notice: t('common.post_writed')
    else
      flash[:error] = t('common.something_went_wrong')
      render :new
    end
  end

  def update
    @post = current_user.
              posts.
              find(params[:id])

    if @post.update_attributes(params[:post])
      redirect_to @post, notice: t('common.post_edited')
    else
      flash[:error] = t('common.something_went_wrong')
      render :edit
    end
  end

  def destroy
    current_user.posts.find(params[:id]).destroy
    flash[:notice] = t('common.post_destroyed')
    redirect_to posts_path
  end
  
end
