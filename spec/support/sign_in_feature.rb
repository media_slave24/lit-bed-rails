module SignInFeature
  def sign_in(user, pass)
    visit new_user_session_path

    within("#login") do
      fill_in "user[login]", with: user.nickname
      fill_in "user[password]", with: pass
      find("input[type=submit]").click
    end
  end
end
