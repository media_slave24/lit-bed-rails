FactoryGirl.define do

  factory :post do
    sequence(:title) { |n| "sometitle#{n}" }
    content 'some content'
    genre 'prose'

    user
    post_type "opus"
  end

  factory :rating do
    vote 1
    post
    user
  end
  
  factory :user do
    sequence(:nickname) { |n| "somenick#{n}" }
    sequence(:email) { |n| "user#{n}@example.com" }
    fullname 'full name'
    password 'trololo'
    password_confirmation 'trololo'
  end
end
