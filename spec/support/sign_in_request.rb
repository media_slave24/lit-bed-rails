module SignInRequest
  def sign_in
    pass = "123213asdasd"
    user = create :user, password: pass, password_confirmation: pass
    post_via_redirect user_session_path, { user: { login: user.nickname, password: pass } }
    request.path.should_not eq(user_session_path)
    user
  end

  def sign_in_admin
    pass = "123213asdasd"
    user = create :user, password: pass, password_confirmation: pass, role: 'admin'
    post_via_redirect user_session_path, 'user[nickname]' => user.nickname, 'user[password]' => pass
    user
  end
end
