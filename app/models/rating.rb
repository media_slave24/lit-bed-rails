class Rating < ActiveRecord::Base
  attr_accessible :vote, :user_id
  validates :post_id, uniqueness: { scope: :user_id }
  validates :post_id, :user_id, presence: true
  validates :vote, exclusion: { :in => [0] }
  belongs_to :post
  belongs_to :user
end
