require 'spec_helper'

describe PostsController do
  before(:all) { create_list :post, 25 }

  context "GET index" do
    before { get :index }

    context "renders 'index'" do
      it { response.should render_template("index") }
    end

    context "assigns @posts" do
      subject(:controller_posts) { assigns :posts }
      subject(:posts) { 
        Post.
        order('created_at DESC').
        paginate(:page => controller.params[:page]) 
      }
      it { controller_posts.should eq(posts) }
    end
  end

  context "GET show" do
    subject(:topic) { Post.first }
    before {get :show, id: topic.id}

    context "renders show" do
      it { response.should render_template("show") }
    end

    context "assigns @post" do
      subject(:controller_post) { assigns :post }

      it { controller_post.id.should eq(topic.id) }
    end
  end
  
  context "GET new" do
    context "when unauthorized" do
      context "redirects" do
        before { get :new }
        it { response.should redirect_to( new_user_session_path ) }
      end
    end

    context "when authorized" do
      let!(:user) { create :user }
      before {
        sign_in user
        get :new
      }
      subject { assigns(:post) }

      context "renders new" do
        it { response.should render_template("new") }
      end

      context "assigns new record for Post" do

        it { should be_new_record }

        context "and assigned post belongs to current user" do
          it { subject.user_id.should eq(user.id) }
        end
      end
    end
  end

  context "GET edit" do
    context "when unauthorized" do
      before { get :edit, id: Post.first.id }

      context "redirects" do
        it { response.should redirect_to(new_user_session_path) }
      end
    end

    context "when authorized" do
      let!(:topic) { Post.first }
      let!(:user) { topic.user }
      before {
        sign_in user
        get :edit, id: topic.id
      }

      context "renders edit" do
        it { response.should render_template('edit') }
      end

      context "assigns @post" do
        subject { assigns :post }
        context "which belongs to current user" do
          it { subject.user_id.should eq(user.id) }
        end

        context "and has right id" do
          it { subject.id.should eq(topic.id) }
        end
      end
    end
  end

  context "POST create" do
    context "when unauthorized" do
      before { 
        post :create 
      }
      context "it redirects to authorization page" do
        it { response.should redirect_to(new_user_session_path) }
      end
    end

    context "when authorized" do
      let!(:params) { { :post => 'Post' } }
      let!(:topic) { mock_model(Post) }
      before {
        user = create :user
        sign_in user
        controller.stub_chain(:current_user, :posts, :build).
          with(params[:post]).
          and_return(topic)
      }

      context "when post is invalid" do
        before {
          topic.should_receive(:save) { false}
          post :create, params
        }

        it { response.should render_template('new')}
      end

      context "when post is valid" do
        before {
          topic.should_receive(:save) { true }
          post :create, params
        }

        it { response.should redirect_to(post_path(topic)) }
      end
    end
  end

  context "POST update" do
    context "when unauthorized" do
      before { put :update, id: Post.first.id }
      context "redirects" do
        it { response.should redirect_to(new_user_session_path) }
      end
    end

    context "when authorized" do
      let!(:topic) { mock_model(Post) }
      let!(:user) { create :user }

      before {
        sign_in user
        controller.stub_chain(:current_user, :posts, :find).
          with(topic.id.to_s).
          and_return(topic)
      }

      context "and params are incorrect" do
        before {
          topic.should_receive(:update_attributes) { false }
          put :update, id: topic.id
        }

        context "renders edit" do
          it { response.should render_template("edit") }
        end
      end

      context "and params are correct" do
        before {
          topic.should_receive(:update_attributes) { true }
          put :update, id: topic.id
        }

        context "redirects to post" do
          it { response.should redirect_to(post_path(topic)) }
        end
      end
    end
  end

  context "DELETE destroy" do
    context "unauthorized user" do
      before {
        delete :destroy, id: Post.first.id
      }

      context "it redirects to authentication path" do
        it { response.should redirect_to(new_user_session_path) }
      end
    end

    context "authorized user" do
      let!(:user) { create :user }
      let!(:topic) { mock_model(Post) }
      before {
        sign_in user 
        controller.stub_chain(:current_user, :posts, :find).
          with(topic.id.to_s).
          and_return(topic)
        topic.should_receive(:destroy)
        delete :destroy, id: topic.id
      }
      it { response.should redirect_to(posts_path) }
    end
  end
  
  context "POST vote" do
    context "authorized user" do
      let!(:user) { create :user }
      let!(:topic) { mock_model(Post) }
      before {
        sign_in user
        user.stub(:posts_rating_sum => 300 )
        controller.stub(:current_user => user)

        Post.stub(:find).with(topic.id.to_s).and_return(topic)

        request.env['HTTP_REFERER'] = 'http://example.com/'
      }
      it { 
        topic.should_receive(:vote).with(4, user.id.to_i)
        post :vote, id: topic.id
      }
      it {
        topic.should_receive(:vote).with(-4, user.id.to_i)
        post :vote, id: topic.id, minus: true
      }
    end
  end

end
