require 'spec_helper'

describe PostsController do
  include SignInRequest

  before(:all) {
    clear_database
  }
  context "vote" do
    let!(:topic) { create :post }

    context "GET must raise error" do
      it { 
        expect {
          get vote_post_up_path(topic.id)
        }.to raise_error(ActionController::RoutingError)
      }
    end

    context "for not-authorized user" do
      it {
        post vote_post_up_path(topic.id)
        response.
          should redirect_to(new_user_session_path)

        topic.
          rating.to_i.should eq(0)
      }
    end

    context "for authorized user" do
      let!(:user) { sign_in }
      it {
        post vote_post_up_path(topic.id), nil, 'HTTP_REFERER' => 'http://example.com/'
        response.
          code.to_i.should eq(302)

        topic.
          reload.
          rating.to_i.should eq(user.level.to_i)
      }
    end
  end
end
