class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :nickname, null: false, limit: 32
      t.string :fullname, null: false, limit: 128
      t.string :encrypted_password
      t.string :email

      t.datetime :reset_password_sent_at
      t.string :reset_password_token

      t.integer :rating, default: 0, null: false

      t.string :role

      t.timestamps
    end
  end
end
