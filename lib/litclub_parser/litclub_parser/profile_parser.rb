#coding: utf-8
class LitclubParser
  class ProfileParser
    def initialize(page)
      @page = page
      @block = page.parsed.css("#content table tr")[0].css('td')[1]
      @poetry = "поез"
      @prose = "проз"
      @else = "інш"
      @endblock = /(?=(#{@poetry}|#{@prose}|#{@else}|(<\s*\/\s*td)))/im
    end

    def info
      [fullname(), prose(), poetry(), other()]
    end

    private

    def fullname
      parsed = @page.parsed
      @block.css('h1')[0].content
    end

    def prose
      parse_content(@prose)
    end

    def poetry
      parse_content(@poetry)
    end

    def other
      parse_content(@else)
    end

    def parse_content(content)
      html = @block.to_s
      scanner = StringScanner.new(html)
      if scanner.skip_until /(?=#{content})/i
        is_div = scanner.skip_until /(?=<\s*div)/i
        raise "No div was found" unless is_div
        content = scanner.scan_until(@endblock)
        result = Nokogiri::HTML(content).css('a').map {|a|
          title = a.content
          link = a.attribute('href').value
          id = link[/(\d+)\D*$/, 1]
          Post.new(title, '', id, link)
        }
        result
      else
        []
      end
    end
  end
end
