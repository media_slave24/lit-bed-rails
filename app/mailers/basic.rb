class Basic < ActionMailer::Base
  default from: "admin@litbed.herokuapp.com"

  def receive(email)
    AdminMail.create from: email.from,
                     subject: email.subject,
                     body: email.body
  end

  def standard(email)
    @content = email[:content]
    mail to:      email[:address], 
         subject: email[:subject],
         from: "admin@litbed.herokuapp.com"
  end
end
