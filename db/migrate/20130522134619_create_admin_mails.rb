class CreateAdminMails < ActiveRecord::Migration
  def change
    create_table :admin_mails do |t|

      t.string :from
      t.string :subject
      t.text :body

      t.timestamps
    end
  end
end
