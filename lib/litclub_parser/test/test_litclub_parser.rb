#coding: utf-8
require File.expand_path('../test_helper', __FILE__)

class TestLitclubParser < Test::Unit::TestCase
  def setup
    @parser = LitclubParser.new
  end

  def test_user_info
    fixture = Fixtures.ne_ta_ale_profile
    @parser.expects(:profile).
                with(fixture.info[:nickname]).
                returns(fixture.html)

    user = @parser.user_info(fixture.info[:nickname])

    assert_equal fixture.info[:nickname], user.nickname
    assert_equal fixture.info[:fullname], user.fullname
    assert_equal 5, user.prose.size
    assert_equal 56, user.poetry.size
    assert_equal 0, user.else.size

    first_prose = user.prose.first
    assert_equal first_prose.title, "Кімната"
    assert_equal first_prose.link, "/texts/show/31087/"
    assert_equal first_prose.id, "31087"

  end

  def test_post
    fixture = Fixtures.prose
    @parser.expects(:post).
              with(fixture.info[:id]).
              returns(fixture.html)

    post = @parser.post_info(fixture.info[:id])
    assert_equal fixture.info[:title], post.title
    assert post.content =~ %r[#{fixture.info[:content]}], "Post content isn't correct"
  end

end
