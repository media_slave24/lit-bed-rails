class SiteConfig < Settingslogic
  source "#{Rails.root}/config/site_config.yml"
  namespace Rails.env
  suppress_errors Rails.env.production?
  load!
end
