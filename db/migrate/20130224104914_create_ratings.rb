class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :vote, null: false
      t.integer :user_id, null: false
      t.integer :post_id, null: false

      t.timestamps
    end
  end
end
