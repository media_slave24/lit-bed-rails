$:.unshift File.dirname(__FILE__)
require 'rubygems'
require 'bundler/setup'
require 'nokogiri'
require 'open-uri'
require 'yaml'
require 'logger'
require 'strscan'
require 'ostruct'
require 'pp'
require 'active_support/dependencies/autoload'
require File.expand_path('../litclub_parser', __FILE__)
require File.expand_path('../test/dumper', __FILE__)

Fixtures = OpenStruct.new
Dir[File.expand_path('../test/fixtures/*', __FILE__)].each do |fixt|
  name = File.basename(fixt)[/^[^\.]+/]
  Fixtures.send :"#{name}=", Dumper.dumper.load(IO.read(fixt))
end

P = LitclubParser.new
