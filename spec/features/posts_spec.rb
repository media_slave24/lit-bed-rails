require 'spec_helper'

describe "Posts" do
  include SignInFeature
  let!(:pass) { 'someeepaas123' }
  let!(:user) { create :user, password: pass, password_confirmation: pass }

  context "#index path" do
    before(:all) { create_list :post, 25, published: true }

    context "displays <SiteConfig.posts.posts_per_page> last posts" do
      before { visit posts_path }
      subject { all('.post') }
      it { subject.size.should be <= SiteConfig.posts.posts_per_page }
      it { subject.each do |post| post.should be_visible end }
    end

    context "it displays voting-links for authenticated user" do
      before { sign_in(user, pass); visit posts_path }
      subject { page }
      it { should have_css(".post .vote .minus") }
      it { should have_css(".post .vote .plus") } 
    end

    context "it not displays voting-links for non-authenticated user" do
      before { visit posts_path }
      subject { page }
      it { should_not have_css(".post .vote .minus") }
      it { should_not have_css(".post .vote .plus") }
    end

    context "it is votable" do
      before { sign_in(user, pass); visit posts_path }
      subject { first('.post') }
      it {
        subject.find('.vote .plus').click
        visit posts_path
        find('.info .rating', text: user.level.to_s)
      }
    end
  end

  context "#new path" do
    before {
      sign_in user, pass
      visit new_post_path
    }
    it {
      title = 'Some' << Time.now.to_i.to_s
      content = 'Somecontent'
      within("#new-post") do
        fill_in "post[title]", with: title
        fill_in "post[content]", with: content
        find("select").select(I18n.t("posts.new.prose"))
        find("input[type=submit]").click
      end

      post = Post.find_by_title(title)
      post.content.should eq(content)
    }
  end
end
