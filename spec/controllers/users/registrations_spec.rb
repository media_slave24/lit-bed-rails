require 'spec_helper'

describe Users::RegistrationsController do
  context "#list" do
    before {
      @request.env["devise.mapping"] = Devise.mappings[:user]
      get :list
    }
    it { response.should render_template("list") }
  end
end
