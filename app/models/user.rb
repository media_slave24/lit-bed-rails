class User < ActiveRecord::Base
  devise  :database_authenticatable, 
          :timeoutable, 
          :registerable,
          :recoverable,
          :omniauthable,
          :omniauth_providers => [:vkontakte, :facebook, :twitter]

  acts_as_messageable required: [:body]
  self.authentication_keys = [:login]

  # Associations
  has_many :posts
  has_many :ratings
  has_many :bookmarks
  has_many :providers

  # Validations
  validates :nickname,
            presence: true,
            uniqueness: true, length: { in: 3..11 }

  validates_presence_of :email, presence: true
  validates_format_of :email, with: /.+@(\w+\.)+\w+/

  validates :password, 
            presence: true, 
            length: { minimum: Rails.env == 'development' ? 1 : 4 }, 
            confirmation: true

  validates :password_confirmation, :fullname,
            presence: true

  validates_attachment :avatar,
    :content_type => { :content_type => ['image/jpg', 'image/jpeg', 'image/png'] }

  # Attributes

  has_attached_file :avatar,
    :styles => { :default => ['84x84#'] },
    :url => ':hash_data/:filename.:extenstion',
    :default_url => "default-avatar.png"

  attr_accessible :nickname, :fullname, :password, 
                  :password_confirmation, :avatar,
                  :email

  attr_accessor :login

  # Class methods

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(nickname) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end

  def self.sort(opts={})
    opts[:rating] ? self.order('rating DESC') : self
  end

  # Instance Methods

  def posts_bookmarks
    posts_ids = bookmarks.map(&:post_id)
    Post.find(posts_ids)
  end

  def has_in_bookmarks?(post_id)
    !!self.bookmarks.find_by_post_id(post_id)
  end

  def voting_level
    Level.new(posts_rating_sum).to_s.to_i
  end
  alias :level :voting_level

  def posts_rating_sum
    self.posts.sum :rating
  end

  def voted?(post_id)
    !!ratings.find_by_post_id(post_id)
  end

  def is_author_of?(post)
    post.user && self.id == post.user.id
  end

  def vk?
    self.provider == 'vkontakte'
  end

  def admin?
    self.role == 'admin'
  end

end
