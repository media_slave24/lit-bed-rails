class LitclubParser
  class PostParser
    def initialize(page)
      @page = page
      @block = page.parsed.css("#content index").children
      @block.delete(@block.at_css('table'))
    end

    def info
      @info ||= [title, content]
    end

    private
    def title
      @block.css("h1").inner_text.strip
    end
    def content
      block = @block.dup
      block.delete(block.at_css('.h1_date'))
      block.delete(block.at_css('h1'))
      block.inner_text.strip
    end
  end
end
