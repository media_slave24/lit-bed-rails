require 'spec_helper'

describe Post do
  before { clear_database }

  context "factory" do
    subject { build :post }
    it { should be_valid }
    it { should be_a(Post) }
  end

  context "without title" do
    subject { build :post, title: nil }
    it { should_not be_valid }
  end

  context "without content" do
    subject { build :post, content: nil }
    it { should_not be_valid }
  end

  context "with incorrect genre" do
    subject { build :post, genre: 'loooooool' }
    it { should_not be_valid }
  end
  
  context "rating" do
    subject { create :post }
    let(:user) { subject.user }

    context "has 'ratings' relation" do
      it { subject.ratings.build.should be_a(Rating) }
    end

    context "is 0 after create" do
      it { subject.rating.should eq(0) }
    end

    context "can be rated" do
      before { subject.vote(1, user.id) }

      it { subject.reload.rating.should eq(1) }
    end

    context "column" do
      subject(:voter) { create :user }
      subject(:post) { create :post }
      it { 
        post.ratings.destroy_all
        post.vote(1, post.user.id)
        post.vote(1, voter.id).should eq(2)
        post.rating.should eq(post.ratings.sum(:vote))
      }
    end

    context "#vote updates users.rating column" do
      let!(:post) { create :post }
      it {
        post.user.rating.should eq(0)
        post.vote(42, post.user.id)
        post.user.rating.should eq(42)
      }
    end
  end

  # context ".sort" do
  #   let(:today) { Time.new.beginning_of_day..Time.new.end_of_day }
  #   let(:yesterday) { Time.new.yesterday.beginning_of_day..Time.new.yesterday.end_of_day }
  #   let(:this_week) { Time.new.beginning_of_week..Time.new.end_of_week }

  #   context "without arguments" do
  #     subject { Post.sort {} }
  #     it { should eq(Post) }
  #   end

  #   context "by time" do
  #     before(:all) {
  #       create_list :post, 5, created_at: proc {|n| today.first + n }
  #       create_list :post, 5, created_at: proc {|n| yesterday.first + n }
  #       create_list :post, 5, created_at: proc {|n| this_week.first + n }
  #       create_list :post, 5, created_at: proc {|n| Time.new.beginning_of_year + n }
  #     }

  #     context "today" do
  #       subject(:today_sorted) { Post.sort time: 'today'}
  #       it {  today_sorted.should eq(Post.all conditions: { created_at: today } ) }
  #     end

  #     context "yesterday" do
  #       subject(:yesterday_sorted) { Post.sort time: 'yesterday' }
  #       it { yesterday_sorted.should eq(Post.all conditions: { created_at: yesterday })  }
  #     end

  #     context "this week" do
  #       subject(:this_week_sorted) { Post.sort time: 'this_week' }
  #       it { this_week_sorted.should eq(Post.all conditions: { created_at: this_week}) } 
  #     end

  #   end

  #   context "by genre" do
  #     it { 
  #       Post::GENRES.each do |genre|
  #         create_list :post, 5, genre: genre
  #         Post.sort(genre: genre).should eq(Post.where(genre: genre))
  #       end
  #     }
  #   end

  #   context "by rating" do
  #     before(:all) { 
  #       Post.destroy_all
  #       create_list :post, 5
  #       Post.all.each do |post|
  #         post.vote(post.id, post.user.id)
  #       end
  #     }
  #     it {
  #       Post.sort(rating: true).should eq( Post.order('rating DESC').all )
  #     }
  #   end

  #   context "together" do
  #     before(:all) {
  #       Post::GENRES.each do |genre|
  #         create_list :post, 5, genre: genre, created_at: proc {|n| today.first + n }
  #         create_list :post, 5, genre: genre, created_at: proc {|n| yesterday.first + n }
  #         create_list :post, 5, genre: genre, created_at: proc {|n| this_week.first + n}
  #       end
  #     }

  #     Post::GENRES.each do |genre|
  #       it { 
  #         Post.
  #         sort(time: 'today', genre: genre).
  #         should eq(Post.all conditions: { created_at: today, genre: genre })
  #       }
  #       it {
  #         Post.
  #         sort(time: 'yesterday', genre: genre).
  #         should eq(Post.all conditions: { created_at: yesterday, genre: genre })
  #       }
  #       it {
  #         Post.
  #         sort(time: 'this_week', genre: genre).
  #         should eq(Post.all conditions: { created_at: this_week, genre: genre })
  #       }
  #     end
  #   end
  # end

  context "#genres_for_select" do
    let!(:post) { build :post }
    it { 
      post.
      genres_for_select.
      should eq( Post::GENRES.map{  |g| [g.humanize, g] } )
    }
    it {
      post.
      genres_for_select { |g| 
      "asd#{g}"
      }.should eq( Post::GENRES.map { |g| ["asd#{g}", g] } )
    }
  end
end
