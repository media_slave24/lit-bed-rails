def rld
  load(File.expand_path(__FILE__))
end

def no_ratings(work)
  work.ratings.destroy_all && work.rating=(0) && work.save if work.is_a?(Work)
end

def w(id=nil)
  id ? Work.find(id) : Work.first
end
