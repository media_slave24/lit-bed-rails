class Users::SessionsController < Devise::SessionsController
  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in) if is_navigational_format?
    sign_in(resource_name, resource)

    respond_to do |f|
      f.html {
        if params[:login_for_omniauth] && params[:provider]
          redirect_to user_omniauth_authorize_path(params[:provider])
        else
          redirect_to after_sign_in_path_for(resource)
        end
      }
    end
  end
end
