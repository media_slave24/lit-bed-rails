RailsApp::Application.routes.draw do

  if Rails.env == 'development'
    get "/app/:action", controller: "application"
  end

  get '/news' => 'news#index', as: "news"

  get '/404' => 'application#render_404'
  get "/rel" => 'application#reload_settings'
  root :to => 'posts#index'

  devise_scope :user do
    get "/users/glogout" => 'users/sessions#destroy', as: "destroy_user_session_get"
    get '/users/show/:id' => 'users/registrations#show', as: 'show_user_registration'
    get '/users/list' => 'users/registrations#list', as: 'list_user_registrations'
  end

  controller "users/messages" do
    get "/messages/inbox", action: "inbox"
    get "/messages/outbox", action: "outbox"
    get "/messages/deleted", action: "deleted"
    get "/messages/write", action: "write"
    get "/messages/:id", action: "read", as: "message"
    post "/messages/send", action: "send_message"
    get "/messages/:id/delete", action: "delete", as: "message_delete"
    get '/messages/:id/perm_delete', action: "permanent_delete", as: "message_delete_permanent"
  end

  resources :posts

  controller "posts" do
    get "/user/:user_id/posts", action: "user_posts", as: "user_posts"
    get "/bookmarks", action: "bookmarks", as: "bookmarks"
    get "/posts/:id/from-bookmarks", action: "destroy_bookmark", as: "post_unbookmark"
    get "/posts/:id/to-bookmarks", action: "create_bookmark", as: "post_bookmark"
    post "/posts/:id/comment", action: "comment", as: "comment_post"
    post "/posts/:id/vote/up", action: "vote", as: 'vote_post_up'
    post "/posts/:id/vote/down", action: "vote", as: 'vote_post_down', minus: true
  end


  # ActiveAdmin.routes(self)
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :users,
    :controllers => { :omniauth_callbacks => 'users/omniauth_callbacks', :sessions => 'users/sessions' },
    :path_names => { :sign_in => "login", :sign_out => "logout", :sign_up => 'register' }

  if Rails.env == 'production'
    match '/:something', :to => "application#render_404", :constraints => { :something => /.*/ }
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
