;(function($) {

	$.noty.layouts.centerUpRight = {
		name: 'centerUpRight',
		options: {},
		container: {
			object: '<ul id="noty_center_up_right_layout_container" />',
			selector: 'ul#noty_center_up_right_layout_container',
			style: function() {
				$(this).css({
					top: '11%',
					left: '45%',
					position: 'fixed',
					width: '50%',
					height: 'auto',
					margin: 0,
					padding: 0,
					listStyleType: 'none',
					zIndex: 9999999
				});
			}
		},
		parent: {
			object: '<li />',
			selector: 'li',
			css: {}
		},
		css: {
			display: 'none'
		},
		addClass: ''
	};

})(jQuery);
