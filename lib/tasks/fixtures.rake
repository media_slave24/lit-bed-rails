namespace :fixtures do
  task :seed => [:environment] do
    print "This action will destroy data, continue? (yes if you ready): "
    answer = STDIN.gets
    if answer.chomp == 'yes'
      puts "Continuing..."
      require 'active_record/fixtures'
      dir = Rails.root.join("db", "fixtures")
      Dir[dir.join("*.yml")].each do |fixture|
        table = File.basename(fixture, ".yml").to_sym
        print "Creating fixtures for #{fixture}..."
        ActiveRecord::Fixtures.create_fixtures File.dirname(fixture),
                                               table
        print "Created!\n"
      end
      puts "Finished"
    else
      puts "Canceled"
    end
  end
end
