ActiveAdmin.register AdminMail do
  index do
    column :from
    column :subject
    column :body
    default_actions
  end  
end
