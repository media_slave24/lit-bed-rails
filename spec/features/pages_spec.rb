require 'spec_helper'

shared_examples "common links" do
  context "has login link" do
    it { page.should have_css("a[href='/users/login']") }
  end

  context "has register link" do
    it { page.should have_css("a[href='/users/register']") }
  end
end

describe "Page" do
  context "Root" do
    before { visit root_path }
    include_examples "common links"
  end

  context "Posts#Index", :links do
    before { visit posts_path }
    include_examples "common links"
  end
end
