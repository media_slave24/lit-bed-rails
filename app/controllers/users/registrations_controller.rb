class Users::RegistrationsController < Devise::RegistrationsController
  def create
    build_resource

    if resource.save
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_up(resource_name, resource)

        respond_to do |f|
          f.html { redirect_to after_sign_up_path_for(resource) }
          f.json { render json: { status: true, message: t('devise.registrations.signed_up') } }
        end
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      # respond_with resource
      respond_to do |f|
        f.html { render resource }
        f.json { render json: { status: false, errors: resource.errors.full_messages } }
      end
    end
  end
  
  def show
    @user = User.find(params[:id])
  end

  def list
    @users = User.sort(rating: true)
  end
end
