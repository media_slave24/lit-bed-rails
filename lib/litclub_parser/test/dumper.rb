class Dumper
  Page = Struct.new(:name, :path, :info, :html) do
    def get_html
      raise "No name or path" unless name && path
      uri = URI("http://litclub.org.ua#{path}")
      puts "Fetching html for #{uri}", ""
      self.html = open(uri).read
      raise "No html fetched" unless self.html
    end
  end

  def initialize(pages)
    puts "--- START DUMPER --- "
    @pages = pages.map{ |ary|
      page = Page.new(*ary)
      puts "Dumping page '#{page.name}', with path '#{page.path}'"
      page.get_html
      page
    }
  end

  def self.dumper
    YAML
  end

  def dump
    puts "Serializing pages with #{self.class.dumper.inspect}"
    puts ""
    @pages.each do |page|
      fname = File.expand_path("../fixtures/#{page.name}.dump", __FILE__)
      File.open(fname, "w") { |f|
        puts "Saving '#{page.name}' into #{fname}"
        f.write(self.class.dumper.dump(page))
      }
    end
    puts ""
    puts "--- FINISH DUMPER --- "
  end
end

