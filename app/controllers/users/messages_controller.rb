class Users::MessagesController < ApplicationController
  before_filter :authenticate_user!

  #With views
  def inbox
    @messages = current_user.received_messages
  end

  def outbox
    @messages = current_user.sent_messages
  end

  def deleted
    @messages = current_user.deleted_messages
  end

  def write
  end

  def read
    @message = current_user.messages.find(params[:id])
    render_404 unless @message
  end

  #Without views
  def send_message
    recipient_nicknames = separate_by_comma params[:recipients]
    recipients = User.
      find_all_by_nickname(recipient_nicknames)

    recipients.each do |recipient|
      msg = current_user.send_message(recipient, params[:body] )
    end
    flash[:alert] = t('common.messages.need_any_recipient') if recipients.empty?
    redirect_to :back
  end

  def delete
    message = current_user.messages.find(params[:id])
    current_user.delete_message(message)
    redirect_to :back
  end

  def permanent_delete
    message = current_user.deleted_messages.find(params[:id])
    current_user.delete_message(message)
    redirect_to :back
  end

  private
  def separate_by_comma(string)
    string.
      split(',').
      map{|i|i.strip}
  end
end
