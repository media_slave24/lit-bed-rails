module ApplicationHelper

  def debug_args(*args)
    content_tag(:pre, :style => 'background:white;font-size:13px;color:black;') do
      args.map {|e| e.to_yaml}.join("\n\n")
    end if Rails.env == 'development'
  end

  def render_js_notifications(messages, opts={})
    messages.map! { |msg|
      {text: msg}.merge(opts).to_json
    }
    content_for :activerecord_js_messages do
      render partial: "shared/js_notifications", object: messages
    end
  end

  def render_nojs_notifications(messages)
    render partial: "shared/nojs_notifications", object: messages
  end

  def random(*args)
    args[ rand(args.size) ]
  end
end
