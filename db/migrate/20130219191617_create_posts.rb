class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title, null: false, limit: 256
      t.text :content, null: false
      t.string :genre, null: false, limit: 32
      t.integer :user_id, null: false
      t.text :annotation
      t.integer :rating, null: false, default: 0
      
      t.boolean :published, null: false, default: false

      t.string :post_type, limit: 32, null: false

      t.timestamps
    end
  end
end
