require File.expand_path("../glue", __FILE__)
WebMock.allow_net_connect!
User.destroy_all
Post.destroy_all

class UserCreator
  def initialize(model, parsed)
    @model = model
    @parsed = parsed
    puts "Initialized syncer"
  end
  def sync
    @parsed.prose.each do |p|
      puts "creating prose alalogue of #{p.title}"
      p.fetch_content
      _post = @model.posts.create do |post|
        post.title = p.title
        post.content = p.content
        post.genre = 'prose'
      end
      raise if !_post.persisted?
      puts "Created!",""
    end

    @parsed.poetry.each do |p|
      puts "Start syncing poetry #{p.title}"
      p.fetch_content
      @model.posts.create do |post|
        post.title = p.title
        post.content = p.content
        post.genre = 'poetry'
      end
      puts "Created poetry with title #{p.title}"
    end
  end
end

users = ["bratlibo"]
puts "Fetcing users: #{users.inspect}",''

users.each do |u|
  parsed = P.user_info(u)
  puts "Having parsed #{parsed}"
  model = User.create do |usr|
    usr.fullname = parsed.fullname
    usr.nickname = parsed.nickname
    usr.email = parsed.nickname + "@mail.ru"
    usr.password = "asdasd"
    usr.password_confirmation = "asdasd"
  end
  raise "Model is not persisted" if !model.persisted?
  puts "Having model #{model}", ""
  puts "Syncing.."
  creator = UserCreator.new(model, parsed)
  creator.sync
end
