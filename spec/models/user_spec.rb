require 'spec_helper'

describe User do
  before { User.destroy_all; Post.destroy_all }
  context "factory" do
    let(:user) { build(:user) }
    it { user.should be_valid }
  end

  context "is incorrect" do
    context "without nickname" do
      subject { build(:user, nickname: nil) }
      it { should_not be_valid }
    end

    context "withour password" do
      subject { build(:user, password: nil) }
      it { should_not be_valid }
    end

    context "without password confirmation" do
      subject { 
        build(:user, 
              password: 'trololo', 
              password_confirmation: nil)
      }

      it { should_not be_valid }
    end

    context "with doubled nicknames" do
      let(:nickname) { 'somenick' }
      before { create(:user, nickname: nickname) }

      it {
        build(:user,
              nickname: nickname).should_not be_valid
      }
    end
  end

  context "#posts_rating_sum" do
    before { User.destroy_all; Post.destroy_all }
    let(:posts_count) { 10 }
    subject(:voter) { create :user }
    subject(:hero) { create :user }
    before {
      create_list :post, posts_count, user_id: hero.id
      hero.posts.each do |post|
        post.vote(10, voter.id)
      end
    }
    it { hero.posts_rating_sum.should eq(100) }
  end

  context "#voting_level" do
    before { 
      User.destroy_all
      Post.destroy_all
    }
    let!(:user) { create :user }

    context "when 0..49 votes" do
      it "is 1" do
        user.stub(:posts_rating_sum => 2)
        user.voting_level.to_i.should eq(1)
        
        user.stub(:posts_rating_sum => 30)
        user.voting_level.to_i.should eq(1)

        user.stub(:posts_rating_sum => 49)
        user.voting_level.to_i.should eq(1)
      end
    end

    context "when 50..99 votes" do
      it "is 2" do
        user.stub :posts_rating_sum => 50
        user.voting_level.to_i.should eq(2)

        user.stub :posts_rating_sum => 99
        user.voting_level.to_i.should eq(2)
      end
    end

    context "when 100..199 votes" do
      it "is 3" do
        user.stub :posts_rating_sum => 100
        user.
          voting_level.
          to_i.
          should eq(3)

        user.stub :posts_rating_sum => 199
        user.
          voting_level.
          to_i.
          should eq(3)
      end
    end

    context "when 200..399 votes" do
      it "is 4" do
        user.stub :posts_rating_sum => 200
        user.voting_level.
          to_i.
          should eq(4)

        user.stub :posts_rating_sum => 399
        user.level.
          to_i.
          should eq(4)
      end
    end

    context "when >=500" do
      it "is 5" do
        user.stub :posts_rating_sum => 500
        user.voting_level.
          to_i.
          should eq(5)

        user.stub :posts_rating_sum => 9999
        user.level.
          to_i.
          should eq(5)
      end
    end
  end

  context "#admin?" do
    let!(:user) { create :user }
    let!(:admin) { create :user, role: 'admin' }
    it { user.should_not be_admin }
    it { admin.should be_admin  }
  end

  context "#voted?" do
    let!(:post) { create :post }
    context "should be false, if not voted" do
      it { post.user.voted?(post.id).should be_false }
    end

    context "should be true if voted" do
      before { post.vote(1, post.user.id) }
      it { post.user.voted?(post.id).should be_true }
    end
  end

  context ".sort" do
    context "by rating" do
      before {
        Post.destroy_all
        User.destroy_all
        create_list :post, 5
        Post.all.each do |post|
          post.vote(post.id, post.user.id)
        end
      }
      it { User.sort(rating: true).should eq(User.order('rating DESC').all) }
    end
  end
end
