module Users::RegistrationsHelper
  def ukr_count(number, opts={})
    number = number.to_i
    return false if number < 1
    number = number.to_s[/\d\d$/].to_i if number > 100
    case number
      when 1
        return opts[:first] || :first
      when 2,3,4
        return opts[:second] || :second
      else
        return opts[:else] || :else
    end
  end
end
