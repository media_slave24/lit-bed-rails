class User
  class Level
    RANGES = {
      1 => (0...50),
      2 => (50...100),
      3 => (100...200),
      4 => (200...500),
      5 => (500...99999999999)
    }
    def initialize(rating_sum)
      @level = detect_level(rating_sum)
    end

    def to_s
      @level
    end

    def to_i
      self.to_s.to_i
    end

    private

    def detect_level(rating_sum)
      level = RANGES.detect { |level, range| range.include?(rating_sum) }
      if level.nil?
        rating_sum > 500 ? 5 : 1
      else
        level[0]
      end
    end
  end
end
