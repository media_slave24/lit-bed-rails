module Database
  def clear_database
    Post.destroy_all
    Rating.destroy_all
    Comment.destroy_all
    User.destroy_all
  end
end
